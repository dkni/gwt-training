package com.gp.training.web.shared.service;

import com.gp.training.web.shared.model.BookingProxy;
import com.gp.training.web.shared.model.GuestProxy;
import org.springframework.web.bind.annotation.RequestBody;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by DK on 07.02.2016.
 */
@Path("/book")
public interface BookingResource {
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    BookingProxy book (@RequestBody BookingProxy proxy);

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    void addGuests (List<GuestProxy> guests);
}
