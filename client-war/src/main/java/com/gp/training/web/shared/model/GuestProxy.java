package com.gp.training.web.shared.model;

import org.jboss.errai.common.client.api.annotations.Portable;

/**
 * Created by DK on 07.02.2016.
 */
@Portable
public class GuestProxy {
    private String firstName;
    private String lastName;
    private int age;
    private String email;
    private int bookingId;

    public GuestProxy(){

    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getBookingId() {
        return bookingId;
    }

    public void setBookingId(int bookingId) {
        this.bookingId = bookingId;
    }
}
