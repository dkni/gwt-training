package com.gp.training.web.client.ui.customers;

import com.google.gwt.core.client.Callback;
import com.gp.training.web.client.params.PageParams;
import com.gp.training.web.client.ui.common.Presenter;
import com.gp.training.web.shared.model.BookingProxy;
import com.gp.training.web.shared.model.GuestProxy;

import java.util.List;

/**
 * Created by DK on 07.02.2016.
 */
public interface CustomersPresenter extends Presenter {
    BookingProxy book(BookingProxy proxy, Callback<BookingProxy, Void> originCallback);
    void addGuests(PageParams params, Callback<List<GuestProxy>, Void> originCallback);
}
