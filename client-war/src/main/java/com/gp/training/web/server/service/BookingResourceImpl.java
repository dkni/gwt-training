package com.gp.training.web.server.service;

import com.gp.training.bizlogic.api.model.BookingDTO;
import com.gp.training.web.shared.model.BookingProxy;
import com.gp.training.web.shared.model.GuestProxy;
import com.gp.training.web.shared.service.BookingResource;
import com.gp.training.bizlogic.client.resource.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by DK on 07.02.2016.
 */
@Service
public class BookingResourceImpl implements BookingResource {

    @Autowired
    BookingService bookingService;

    @Override
    public BookingProxy book(BookingProxy proxy) {
        BookingDTO dto = new BookingDTO();
        dto.setOfferId(proxy.getOfferId());
        dto.setGuestCount(proxy.getGuestCount());
        dto.setStartDate(proxy.getStartDate());
        dto.setEndDate(proxy.getEndDate());
        dto = bookingService.createBooking(dto);
        BookingProxy newProxy = new BookingProxy();
        proxy.setId(dto.getId());
        return newProxy;
    }

    @Override
    public void addGuests(List<GuestProxy> guests) {

    }
}
