package com.gp.training.web.shared.service;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.gp.training.web.shared.model.OfferProxy;

@Path("/search")
public interface AvailabilityResource {
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<OfferProxy> getOffers(@QueryParam("cityId") int cityId,
				@QueryParam("guestCount") int guestCount, 
				@QueryParam("startDate") String startDate, 
				@QueryParam("endDate") String endDate);
}
