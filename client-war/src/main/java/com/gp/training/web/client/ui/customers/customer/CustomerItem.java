package com.gp.training.web.client.ui.customers.customer;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.sksamuel.jqm4gwt.form.elements.JQMText;

public class CustomerItem extends Composite {

	private static CustomerItemUiBinder uiBinder = GWT.create(CustomerItemUiBinder.class);
	@UiField
	protected JQMText firstName;
	@UiField
	protected JQMText lastName;
	@UiField
	protected JQMText age;
	@UiField
	protected JQMText email;

	public JQMText getFirstName() {
		return firstName;
	}

	public JQMText getLastName() {
		return lastName;
	}

	public JQMText getAge() {
		return age;
	}

	public JQMText getEmail() {
		return email;
	}

	interface CustomerItemUiBinder extends UiBinder<Widget, CustomerItem> {
	}

	public CustomerItem() {
		initWidget(uiBinder.createAndBindUi(this));
	}

}
