package com.gp.training.web.client.ui.customers;

import com.google.gwt.core.client.Callback;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Widget;
import com.gp.training.web.client.app.AppContext;
import com.gp.training.web.client.app.PageToken;
import com.gp.training.web.client.params.PageParams;
import com.gp.training.web.client.ui.common.GenericBaseView;
import com.gp.training.web.client.ui.common.PageBaseView;
import com.gp.training.web.client.ui.customers.customer.CustomerItem;
import com.gp.training.web.shared.model.BookingProxy;
import com.gp.training.web.shared.model.GuestProxy;
import com.sksamuel.jqm4gwt.button.JQMButton;
import com.sksamuel.jqm4gwt.list.JQMList;
import com.sksamuel.jqm4gwt.list.JQMListItem;

import java.util.ArrayList;
import java.util.List;

public class CustomersView extends GenericBaseView<CustomersPresenterImpl> implements PageBaseView {
	
	private PageParams pageParams;
	private Callback<BookingProxy, Void> originCallback;

	@UiField
	protected JQMButton nextBtn;
	@UiField
	protected JQMList guestList;

	private static CustomersViewUiBinder uiBinder = GWT.create(CustomersViewUiBinder.class);

	interface CustomersViewUiBinder extends UiBinder<Widget, CustomersView> {
	}

	public CustomersView() {
		initWidget(uiBinder.createAndBindUi(this));
		setPresenter(new CustomersPresenterImpl());
		initActions();
		
	}
	
	
	private void initActions() {
		nextBtn.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				PageParams newParams = buildNewParams();
				BookingProxy proxy = new BookingProxy();
				proxy.setOfferId(newParams.getOfferId());
				proxy.setGuestCount(newParams.getGuestCount());
				proxy.setStartDate(newParams.getStartDate());
				proxy.setEndDate(newParams.getEndDate());
				getPresenter().book(proxy, originCallback);
				AppContext.navigationService.next(PageToken.BOOKING, newParams);
			}
		});
	}


	@Override
	public void loadPageParams(PageParams params) {
		pageParams = params != null ? params : new PageParams(); 
		recreateGuestsList();
	}
	
	private void recreateGuestsList() {
		guestList.clear();
		
		int guestCount = pageParams.getGuestCount();
		
		for (int i=0; i<guestCount; i++) {
			JQMListItem listItem = new JQMListItem();
			listItem.setControlGroup(true, false);
			
			CustomerItem customer = new CustomerItem();
			listItem.addWidget(customer);
			
			guestList.appendItem(listItem);
		}
		
		guestList.recreate();
		guestList.refresh();
	}

	private PageParams buildNewParams(){
		PageParams newParams = new PageParams();
		newParams.setOfferId(pageParams.getOfferId());
		newParams.setGuestCount(pageParams.getGuestCount());
		newParams.setStartDate(pageParams.getStartDate());
		newParams.setEndDate(pageParams.getEndDate());
		List<GuestProxy> guests = new ArrayList<>();
		for (JQMListItem item : guestList.getItems()) {
			for (int i = 0; i < item.getWidgetCount(); i++) {
				CustomerItem guestItem = (CustomerItem) item.getWidget(i);
				GuestProxy guest = new GuestProxy();
				guest.setFirstName(String.valueOf(guestItem.getFirstName().getValue()));
				guest.setLastName(String.valueOf(guestItem.getLastName().getValue()));
				guest.setAge(Integer.valueOf(String.valueOf(guestItem.getAge().getValue())));
				guest.setEmail(String.valueOf(guestItem.getEmail().getValue()));
				guests.add(guest);
			}
		}
		newParams.setGuests(guests);
		return newParams;
	}
}
