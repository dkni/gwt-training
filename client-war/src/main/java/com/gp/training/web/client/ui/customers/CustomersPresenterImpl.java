package com.gp.training.web.client.ui.customers;

import com.google.gwt.core.client.Callback;
import com.gp.training.web.client.params.PageParams;
import com.gp.training.web.shared.model.BookingProxy;
import com.gp.training.web.shared.model.GuestProxy;
import com.gp.training.web.shared.service.BookingResource;
import org.jboss.errai.common.client.api.RemoteCallback;
import org.jboss.errai.enterprise.client.jaxrs.api.RestClient;

import java.util.List;

/**
 * Created by DK on 07.02.2016.
 */
public class CustomersPresenterImpl implements CustomersPresenter {

    @Override
    public BookingProxy book(BookingProxy proxy, final Callback<BookingProxy, Void> originCallback) {
        return RestClient.create(BookingResource.class, new RemoteCallback<BookingProxy>() {
            @Override
            public void callback(BookingProxy response) {
                if (originCallback != null) originCallback.onSuccess(response);
            }
        }).book(proxy);
    }

    @Override
    public void addGuests(PageParams params, Callback<List<GuestProxy>, Void> originCallback) {

    }
}
