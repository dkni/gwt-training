package com.gp.training.web.client.params;

import com.gp.training.web.shared.model.GuestProxy;

import java.util.Date;
import java.util.List;

public class PageParams {
	
	private int guestCount;
	private int offerId;
	private Date startDate;
	private Date endDate;
	private List<GuestProxy> guests;
	
	public int getGuestCount() {
		return guestCount;
	}
	public void setGuestCount(int guestCount) {
		this.guestCount = guestCount;
	}
	public int getOfferId() {
		return offerId;
	}
	public void setOfferId(int offerId) {
		this.offerId = offerId;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public List<GuestProxy> getGuests() {
		return guests;
	}

	public void setGuests(List<GuestProxy> guests) {
		this.guests = guests;
	}
}
