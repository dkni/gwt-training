package com.gp.training.web.client.ui.search.offer;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.gp.training.web.shared.model.OfferProxy;
import com.sksamuel.jqm4gwt.form.elements.JQMCheckbox;
import com.sksamuel.jqm4gwt.html.Div;

public class OfferDescItem extends Composite {

	private static OfferDescItemUiBinder uiBinder = GWT.create(OfferDescItemUiBinder.class);

	interface OfferDescItemUiBinder extends UiBinder<Widget, OfferDescItem> {
	}
	
	private OfferProxy offerProxy;
	
	@UiField
	protected Div hotelNameField;
	@UiField
	protected Div roomNameField;
	@UiField
	public JQMCheckbox selectOfferCheckBox;

	public OfferDescItem() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	public void populate(OfferProxy offerProxy) {
		this.offerProxy = offerProxy;
		
		hotelNameField.setText(offerProxy.getHotel().getName());
		roomNameField.setText(offerProxy.getRoom().getName());
	}
	
	public OfferProxy getOfferProxy() {
		return offerProxy;
	}
}
