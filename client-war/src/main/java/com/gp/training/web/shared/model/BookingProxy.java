package com.gp.training.web.shared.model;

import org.jboss.errai.common.client.api.annotations.Portable;

import java.awt.print.Book;
import java.util.Date;

/**
 * Created by DK on 07.02.2016.
 */
@Portable
public class BookingProxy {
    private int id;
    private int offerId;
    private int guestCount;
    private Date startDate;
    private Date endDate;

    public BookingProxy(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOfferId() {
        return offerId;
    }

    public void setOfferId(int offerId) {
        this.offerId = offerId;
    }

    public int getGuestCount() {
        return guestCount;
    }

    public void setGuestCount(int guestCount) {
        this.guestCount = guestCount;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
