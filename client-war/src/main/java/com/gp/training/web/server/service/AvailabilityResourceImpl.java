package com.gp.training.web.server.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gp.training.bizlogic.api.model.HotelDTO;
import com.gp.training.bizlogic.api.model.OfferDTO;
import com.gp.training.bizlogic.api.model.RoomTypeDTO;
import com.gp.training.bizlogic.client.resource.OfferService;
import com.gp.training.web.shared.model.HotelProxy;
import com.gp.training.web.shared.model.OfferProxy;
import com.gp.training.web.shared.model.RoomTypeProxy;
import com.gp.training.web.shared.service.AvailabilityResource;

@Component
public class AvailabilityResourceImpl implements AvailabilityResource {
	
	@Autowired
	private OfferService offerService;
	
	@Override
	public List<OfferProxy> getOffers(int cityId, int guestCount, String startDate, String endDate) {
		
		List<OfferDTO> offers = offerService.getOffers(cityId, guestCount, startDate, endDate);
		
		List<OfferProxy> offerProxies = new ArrayList<>(offers.size());
		
		for (OfferDTO offerDTO : offers) {
			OfferProxy proxy = new OfferProxy();
			proxy.setOfferId(offerDTO.getOfferId());
			
			HotelDTO hotelDTO = offerDTO.getHotel();
			HotelProxy hotelProxy = new HotelProxy();
			if (hotelDTO != null) {
				hotelProxy.setCode(hotelDTO.getCode());
				hotelProxy.setName(hotelDTO.getName());
				if (hotelDTO.getId() != 0)  {
					hotelProxy.setId(hotelDTO.getId());
				}
			}
			proxy.setHotel(hotelProxy);
			
			RoomTypeProxy roomProxy = new RoomTypeProxy();
			RoomTypeDTO roomDTO = offerDTO.getRoom();
			
			if (roomDTO != null) {
				roomProxy.setCode(roomDTO.getCode());
				roomProxy.setName(roomDTO.getName());
				if (roomDTO.getId() != 0) {
					roomProxy.setId(roomDTO.getId());
				}
			}	
			proxy.setRoom(roomProxy);
			
			offerProxies.add(proxy);
		}
		
		return offerProxies;
	}

}
