package com.gp.training.bizlogic.client.resource;

import com.gp.training.bizlogic.api.model.BookingDTO;
import com.gp.training.bizlogic.api.model.GuestDTO;

import java.util.List;

public interface BookingService {
	
	BookingDTO createBooking(BookingDTO booking);
	List<GuestDTO> addGuests(List<GuestDTO> guests);
}
