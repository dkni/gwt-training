package com.gp.training.bizlogic.client.resource;

import com.gp.training.bizlogic.api.model.GuestDTO;
import com.gp.training.bizlogic.api.params.BookingCreateParams;
import com.gp.training.bizlogic.api.resource.BookingResource;
import com.gp.training.bizlogic.client.ClientSender;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.springframework.beans.factory.annotation.Autowired;
import com.gp.training.bizlogic.api.model.BookingDTO;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BookingServiceImpl implements BookingService {

	@Autowired
	ClientSender sender;

	@Override
	public BookingDTO createBooking(BookingDTO booking) {
		ResteasyWebTarget webTarget = sender.getRestEasyTarget();
		BookingResource bookingResource = webTarget.proxy(BookingResource.class);
		BookingCreateParams params = new BookingCreateParams.Builder()
				.offerId(booking.getOfferId())
				.guestCount(booking.getGuestCount())
				.startDate(booking.getStartDate())
				.endDate(booking.getEndDate()).build();

		BookingDTO retrieved = bookingResource.create(params);
		webTarget.getResteasyClient().close();
		return retrieved;
	}

	@Override
	public List<GuestDTO> addGuests(List<GuestDTO> guests) {
		return null;
	}
}
