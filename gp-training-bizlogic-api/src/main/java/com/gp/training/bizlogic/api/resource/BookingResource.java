package com.gp.training.bizlogic.api.resource;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.gp.training.bizlogic.api.model.BookingDTO;
import com.gp.training.bizlogic.api.params.BookingCreateParams;

@Path("/book")
public interface BookingResource {
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	BookingDTO create(BookingCreateParams params);
}
