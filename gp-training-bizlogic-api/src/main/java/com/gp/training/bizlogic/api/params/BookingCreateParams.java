package com.gp.training.bizlogic.api.params;

import java.util.Date;

public class BookingCreateParams {
	
	private int offerId;
	private int guestCount;
	private Date startDate;
	private Date endDate;
	
	public static class Builder {
		private int offerId;
		private int guestCount;
		private Date startDate;
		private Date endDate;
		
		public Builder offerId(int offerId) {
			this.offerId = offerId;
			return this;
		}
		
		public Builder guestCount(int guestCount) {
			this.guestCount = guestCount;
			return this;
		}
		
		public Builder startDate(Date startDate) {
			this.startDate = startDate;
			return this;
		}
		
		public Builder endDate(Date endDate) {
			this.endDate = endDate;
			return this;
		}
		
		public BookingCreateParams build() {
			BookingCreateParams params = new BookingCreateParams();
			params.offerId = offerId;
			params.guestCount = guestCount;
			params.startDate = startDate;
			params.endDate = endDate;
			return params;
		}
	}

	public int getOfferId() {
		return offerId;
	}

	public int getGuestCount() {
		return guestCount;
	}

	public Date getStartDate() {
		return startDate;
	}

	public Date getEndDate() {
		return endDate;
	}
}
