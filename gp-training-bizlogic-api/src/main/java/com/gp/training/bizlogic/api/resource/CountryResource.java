package com.gp.training.bizlogic.api.resource;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.gp.training.bizlogic.api.model.CountryDTO;


@Path("/country")
public interface CountryResource {
	
	@GET
	@Path("{countryId}")
	@Produces(MediaType.APPLICATION_JSON)
	public CountryDTO getCountry(@PathParam("countryId") int countryId);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<CountryDTO> getCountries();
}
