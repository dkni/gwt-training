package com.gp.training.bizlogic.api.resource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.gp.training.bizlogic.api.model.HotelInfoDTO;


@Path("/hotelInfo")
public interface HotelInfoResource {
	
	@GET
	@Path("{hotelId}")
	@Produces(MediaType.APPLICATION_JSON)
	public HotelInfoDTO getHotelInfo(@PathParam("hotelId") int hotelId);
}
