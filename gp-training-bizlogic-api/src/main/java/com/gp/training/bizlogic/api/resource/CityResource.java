package com.gp.training.bizlogic.api.resource;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.gp.training.bizlogic.api.model.CityDTO;

@Path("/city")
public interface CityResource {
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{cityId}")
	public CityDTO getCity(@PathParam("cityId") int cityId);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<CityDTO> getCities();
}
