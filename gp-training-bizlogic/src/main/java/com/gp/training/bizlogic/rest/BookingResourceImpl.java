package com.gp.training.bizlogic.rest;

import com.gp.training.bizlogic.domain.Booking;
import com.gp.training.bizlogic.domain.Offer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gp.training.bizlogic.api.model.BookingDTO;
import com.gp.training.bizlogic.api.params.BookingCreateParams;
import com.gp.training.bizlogic.api.resource.BookingResource;
import com.gp.training.bizlogic.dao.BookingDao;

@Component
public class BookingResourceImpl implements BookingResource {
	
	@Autowired
	private BookingDao bookingDao;
	
	@Override
	public BookingDTO create(BookingCreateParams params) {
		Booking booking = new Booking();
		Offer offer = new Offer();
		offer.setId(params.getOfferId());
		booking.setOffer(offer);
		booking.setGuestCount(params.getGuestCount());
		booking.setStartDate(params.getStartDate());
		booking.setEndDate(params.getEndDate());
		booking = bookingDao.create(booking);

		BookingDTO dto = new BookingDTO();
		dto.setId(booking.getId());
		return dto;
	}
}
