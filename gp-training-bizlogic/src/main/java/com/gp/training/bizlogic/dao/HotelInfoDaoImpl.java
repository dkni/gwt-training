package com.gp.training.bizlogic.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import com.gp.training.bizlogic.api.model.HotelInfoDTO;

@Service
public class HotelInfoDaoImpl implements HotelInfoDao {
	
	private static final String REQUEST_HOTEL_INFO_BY_ID = "SELECT * FROM hotel_info  where id=?";
	
	@Resource(name = "jdbcTemplate")
    private JdbcTemplate jdbcTemplate;
	
	@Override
	public HotelInfoDTO getHotelInfo(int id) {
		
		final HotelInfoDTO hotelInfo = jdbcTemplate.queryForObject(REQUEST_HOTEL_INFO_BY_ID, new RowMapper<HotelInfoDTO>() {
            @Override
            public HotelInfoDTO mapRow(ResultSet rs, int i) throws SQLException {
            	HotelInfoDTO hotel = new HotelInfoDTO();
            	hotel.setId(rs.getInt("id"));
            	hotel.setName(rs.getString("name"));
            	hotel.setCode(rs.getString("code"));
            	hotel.setDescription("description");           	
                return hotel;
            }
        }, id);
      
		return hotelInfo;		
	}
	
}
