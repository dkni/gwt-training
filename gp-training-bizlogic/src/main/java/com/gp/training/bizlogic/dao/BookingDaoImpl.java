package com.gp.training.bizlogic.dao;

import org.springframework.stereotype.Repository;

import com.gp.training.bizlogic.domain.Booking;

import javax.transaction.Transactional;

@Repository
public class BookingDaoImpl extends GenericDaoImpl<Booking, Integer> implements BookingDao {
	
	public BookingDaoImpl(){
		super(Booking.class);
	}
	
	@Override
	public Booking create(Booking booking) {
		em.persist(booking);
		em.flush();
		return booking;
	}
	
}
