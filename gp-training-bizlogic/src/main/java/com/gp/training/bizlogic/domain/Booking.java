package com.gp.training.bizlogic.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="booking")
public class Booking extends AbstractEntity {
	@Column(name="guests")
	private int guestCount;

	@Column(name="checkIn")
	private Date startDate;

	@Column(name="checkOut")
	private Date endDate;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="offer_id")
	private Offer offer;

	public int getGuestCount() {
		return guestCount;
	}

	public void setGuestCount(int guestCount) {
		this.guestCount = guestCount;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Offer getOffer() {
		return offer;
	}

	public void setOffer(Offer offer) {
		this.offer = offer;
	}
	
}
