package com.gp.training.bizlogic.dao;

import com.gp.training.bizlogic.api.model.HotelInfoDTO;

public interface HotelInfoDao {
	
	public HotelInfoDTO getHotelInfo(int id);
}
