package com.gp.training.bizlogic.dao;

import com.gp.training.bizlogic.domain.Booking;

public interface BookingDao extends GenericDao<Booking, Integer> {
	
	Booking create(Booking booking);
}
